package pachet;

public class Rectangle {
	
	
	public double AreaRect(double length,double width)
	{
		double result = 0.0;
		result=length * width;
		System.out.println("The RECTANGLE area is: " + result);
		return 0;
	}
	
	public double PerimRect(double length, double width)
	{
		double result = 0.0;
		result = 2*length + 2*width;
		System.out.println("The RECTANGLE perimeter is: " + result);
		return 0;
	}
}
