package pachet;
import java.util.Scanner;

public class Main {

	
	public static void main(String[] args) {
		System.out.println("Pick : 1.Circle          2.Rectangle          3.Triangle          4.Exit");
		 Scanner scanner = new Scanner(System.in);
		 int choice = scanner.nextInt();
		 
		 
		 switch (choice) {
	        case 1:
	            System.out.println("Give a radius input:\n");
	            Scanner a = new Scanner(System.in);
	            double rad=a.nextInt();
	            Circle cerc = new Circle();
	            cerc.Perim(rad);
	            cerc.Area(rad);
	            a.close();
	            break;
	        case 2:
	        	System.out.println("Give a length input:\n");
	        	Scanner b = new Scanner(System.in);
		        double len=b.nextInt();
	        	System.out.println("Give a width input:\n");
	        	Scanner c = new Scanner(System.in);
		        double wid=c.nextInt();
		        
		        
	            Rectangle rect = new Rectangle();
	            rect.PerimRect(len,wid);
	            rect.AreaRect(len,wid);
	            
	            b.close();
	            c.close();
	            
	            break;
	        case 3:
	        	System.out.println("Give an input for the first side of the triangle:\n");
	        	Scanner d = new Scanner(System.in);
	        	double side1=d.nextInt();
	        	
	        	System.out.println("Give an input for the second side of the triangle:\n");
	        	Scanner e = new Scanner(System.in);
	        	double side2=e.nextInt();
	        	
	        	System.out.println("Give an input for the third side of the triangle:\n");
	        	Scanner f = new Scanner(System.in);
	        	double side3=f.nextInt();
	        	
	        	Triangle triang = new Triangle();
	        	triang.PerimTr(side1, side2, side3);
	        	triang.AreaTr(side1, side2, side3);
	        	
	        	d.close();
	        	e.close();
	        	f.close();
	            break;
	        case 4:
	        	break;
	     
	            
		 }
		 scanner.close();



	}

}
